﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace uploadIMG
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    public partial class MainWindow : Window
    {
        string filePath;    //圖檔位置

        public MainWindow()
        {
            InitializeComponent();
        }

        private void btn_open_Click(object sender, RoutedEventArgs e)
        {
            var openDlg = new Microsoft.Win32.OpenFileDialog();

            //取得程式所在目錄
            string currentpath = Directory.GetCurrentDirectory();
            openDlg.InitialDirectory = currentpath;

            // 過濾檔案類型為JPG
            openDlg.Filter = "JPEG Image(*.jpg)|*.jpg";

            // 開啟檔案視窗
            bool? result = openDlg.ShowDialog(this);
            if (!(bool)result)
            {
                return;
            }

            // 選定的檔案位置
            filePath = openDlg.FileName;

            //顯示圖片位置
            textBox.Text = filePath;
        }

        private void btn_upload_Click(object sender, RoutedEventArgs e)
        {
            // upload img here //
            // using imgurAPI or else API //
                       
        }
    }
}
